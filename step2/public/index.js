import React, {Component} from 'react';
import ReactDOM from 'react-dom';

class Imger extends Component {
  constructor(props) {
    super(props); // gives us context for this
    this.state = {
      url: 'http://placehold.it/350x150',
      alt: 'default'
    };
  }
  update(e) {
    if (e.charCode === 13) {
      this.setState({
        url: e.target.value
      });
    }
  }
  render() {
    return (
      <div>
        <div className="jumbotron">
          <h1>Hello, world!</h1>
          <img src={this.state.url} alt={this.state.alt} className="img-thumbnail" />
          <div className="form-group">
            <label htmlFor="url">Image Link</label>
            <input type="email"
              className="form-control"
              id="url"
              placeholder="http://"
              onKeyPress={this.update.bind(this)}
            />
          </div>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<Imger />, document.getElementById('react-app'));
