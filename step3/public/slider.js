import React, {Component} from 'react';

class Slider extends Component {
  render() {
    return (
      <input className="form-control"
        type="range"
        min="0"
        max="255"
        onChange={this.props.update} />
    );
  }
}

export default Slider;
