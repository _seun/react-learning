import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import Slider from './slider';

const defaultState = {
  red: 120,
  green: 0,
  blue: 60
};

class ColorBoard extends Component {
  constructor(props) {
    super(props);
    this.update = this.updateParentState.bind(this);
    this.state = defaultState;
  }
  updateParentState(color, e) {
    var newState = {[color]: e.target.value};
    this.setState(newState);
  }
  render() {
    var divStyle = {
      background: `rgb(
        ${this.state.red},
        ${this.state.green},
        ${this.state.blue})`
    };
    return (
      <div>
        <div className="jumbotron">
          <div className="row">
            <div style={divStyle} className="board col-sm-7">
            </div>
            <div className="col-sm-5">
              <p>Red</p>
              <Slider update={this.update.bind(this, 'red')}/>
              <p>Green</p>
              <Slider update={this.update.bind(this, 'green')}/>
              <p>Blue</p>
              <Slider update={this.update.bind(this, 'blue')}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<ColorBoard />, document.getElementById('app'));
