import gulp from 'gulp';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import browserify from 'browserify';
import babel from 'babelify';
import react from 'reactify';
import concat from 'gulp-concat';
import sourcemaps from 'gulp-sourcemaps';

gulp.task('copy', () => {
  gulp
    .src('./public/index.html')
    .pipe(gulp.dest('dist'))
});

gulp.task('js', () => {
  console.log('runnning js task');
  var b = browserify('./public/index.js', {
    debug: true
  }).transform(babel, {
    presets: ['es2015', 'react']
  });

  b.bundle()
    .on('error', function (err) {
      console.error(err);
      this.emit('end');
    })
    .pipe(source('index.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist'));

});

var sassPath = './public/**/*.sass';
var jsPath = './public/**/*.js';

gulp.task('watch:sass', function() {
  gulp.watch(sassPath, ['sass']);
});
gulp.task('watch:js', function() {
  gulp.watch(jsPath, ['js', 'copy']);
});

gulp.task('watch', ['watch:sass', 'watch:js']);
gulp.task('build', ['js', 'copy']);
